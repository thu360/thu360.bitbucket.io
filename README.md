# Used Images

| Image                      | Location                | File Name
| -------------------------- | ----------------------- | ----------------------------- |
| IMG_20200824_140847_00_002 | Großer Innenhof         | pws_large_courtyard.jpg       |
| IMG_20200824_141101_00_003 | Foyer B-Bau             | pws_lobby_building_b.jpg      |
| IMG_20200824_141300_00_005 | Eingang B-Bau           | pws_entrance_building_b.jpg   |
| IMG_20200824_141425_00_006 | Eingang Mensa           | pws_entrance_mensa.jpg        |
| IMG_20200824_141738_00_007 | Eingang Bibliothek      | pws_entrance_library.jpg      |
| IMG_20200824_141843_00_008 | Eingang Cafeteria       | pws_entrance_cafeteria.jpg    |
| IMG_20200824_142437_00_012 | Büro Duale Studiengänge | pws_dual_study.jpg            |
| IMG_20200824_142756_00_013 | Büro SSC                | pws_ssc.jpg                   |
| IMG_20200824_142900_00_014 | Foyer A-Bau             | pws_lobby_building_a.jpg      |
| IMG_20200824_143023_00_015 | A-Bau zu C-Bau          | pws_crossing_a_c.jpg          |
| IMG_20200824_143246_00_017 | Flur C-Bau              | pws_floor_building_c.jpg      |
| IMG_20200824_144048_00_019 | Aula Eingang            | pws_auditorium_entrance.jpg   |
| IMG_20200824_144135_00_020 | Aula Front              | pws_auditorium_front.jpg      |
| IMG_20200824_144358_00_021 | THU Studio              | pws_studio.jpg                |
| IMG_20200824_144500_00_022 | Automaten C-Bau         | pws_automata_building_c.jpg   |
| IMG_20200824_150914_00_023 | Eingang V-Bau           | aea_entrance_building_v.jpg   | 
| IMG_20200824_151027_00_024 | Foyer V-Bau             | aea_lobby_building_v.jpg      |
| IMG_20200824_151946_00_025 | Eingang Bibliothek      | aea_entrance_library.jpg      |
| IMG_20200824_152106_00_026 | Eingang Cafeteria       | aea_entrance_cafeteria.jpg    |
| IMG_20200824_152216_00_027 | Mensa Sitzbereich       | aea_mensa_seating.jpg         |
| IMG_20200824_152342_00_029 | Mensa Sitzbereich Außen | aea_mensa_seating_outside.jpg |

# Minimap

The minimap uses the data under "marker" within the config.js configuration file.
The x and y coordinates correspond to the pixel coordinates of the map.
A map must have a resolution of 300x300 pixels, else the marker coordinates do not match the pixel coordinates.
The used maps are loaded in the thu.minimap.init() function in thu.js.

The minimap can also show the view direction. To correct the difference between view direction shown
in the minimap and the view direction on the image, an offset can be defined under "marker" with the
value "rotationOffset". The "rotationOffset" must be given in radians. With the helper function "toRadian"
it is also possible to use degree values. By adding the GET-Parameter "debug=three-six-tea-time", a
visual helper can be activated to determine the correct offset more easily. 

# TODO

- [ ] Create Thumbnails of 360 degree images
    - File names see table above
    - Should be quadratic resolution (best fit: 300x300)
    - Thumbnails are placed in assets/thumbs directory
- [ ] Create 360 degree panorama images
    - File names see table above
    - Images should be placed in assets/images directory
