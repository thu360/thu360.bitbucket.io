var threeSixTea = threeSixTea || {};

threeSixTea.common = {
     getURLParam: function(name) {
        params = new URLSearchParams(window.location.search);
        return params.get(name);
    },   
}

threeSixTea.vr = {
    __scene: null,
    
    changeImage: undefined,
    
    init: function() {
        let iframe = document.getElementById("content-frame");
        let iframeDoc = iframe.contentWindow.document;
        threeSixTea.vr.__scene = iframeDoc.getElementById("image-360");
    },
}

threeSixTea.sidebar = {
    __language: "de",
    __lastSelectedCardId: "menu-0-view-0",

    __onClick: function(cardData) {
        if(threeSixTea.vr.changeImage !== undefined) {
            threeSixTea.vr.changeImage(cardData.image);
        }
        
        window.setTimeout(function() {
            if(threeSixTea.minimap.setMarker !== undefined) {
                let marker = cardData.marker;
                threeSixTea.minimap.displayMap(marker.minimap);
                threeSixTea.minimap.setMarker(marker.x, marker.y);
                threeSixTea.minimap.setViewRotationOffset(marker.rotationOffset);
            }
        }, 505); // 500ms is the duration of the fade out animation +5ms wait for full fade out
    },
        
    
    __createCard: function(cardData) {
        let cardElement = document.createElement("div");
        cardElement.classList.add("card");
        
        let cardCoverElement = document.createElement("img");
        cardCoverElement.classList.add("card-cover");
        cardCoverElement.setAttribute("src", `${threeSixTea.config.resources.thumbsDir}/${cardData.thumb}`);
        cardElement.appendChild(cardCoverElement);
        
        let cardTitleBoxElement = document.createElement("div");
        cardTitleBoxElement.classList.add("card-title-box");
        cardElement.appendChild(cardTitleBoxElement);
        
        let cardTitleElement = document.createElement("div");
        cardTitleElement.classList.add("card-title");
        if(threeSixTea.sidebar.__language in cardData.name) {
            cardTitleElement.innerHTML = cardData.name[threeSixTea.sidebar.__language];
        } else {
            cardTitleElement.innerHTML = cardData.name["de"];
        }
        cardTitleBoxElement.appendChild(cardTitleElement);
        
        if("sub" in cardData) {
            let cardSubTitleElement = document.createElement("div");
            cardSubTitleElement.classList.add("card-sub-title");
            if(threeSixTea.sidebar.__language in cardData.sub) {
                cardSubTitleElement.innerHTML = cardData.sub[threeSixTea.sidebar.__language];
            } else {
                cardSubTitleElement.innerHTML = cardData.sub["de"];
            }
            cardTitleBoxElement.appendChild(cardSubTitleElement);
        }
        
        cardElement.addEventListener("click", function(e) {
            if(threeSixTea.sidebar.__lastSelectedCardId !== null) {
                let cardElement = document.getElementById(threeSixTea.sidebar.__lastSelectedCardId);
                cardElement.classList.remove('active');
            }
            
            threeSixTea.sidebar.__lastSelectedCardId = cardElement.id;
            cardElement.classList.add('active');
            threeSixTea.sidebar.__onClick(cardData);
        });
        
        cardElement.addEventListener("mouseenter", function(e) {
            if(threeSixTea.minimap.displayPotentialMarker !== undefined) {
                threeSixTea.minimap.displayPotentialMarker(cardData.marker);
            }
        });
        
        cardElement.addEventListener("mouseleave", function(e) {
            if(threeSixTea.minimap.hidePotentialMarker !== undefined) {
                threeSixTea.minimap.hidePotentialMarker(cardData.marker);
            }
        });
                
        return cardElement;
    },
    
    __createMenu: function(menu_index, menuConfig) {
        let sideMenuElement = document.createElement("div");
        sideMenuElement.classList.add("sidemenu");
        
        let sideMenuTitleElement = document.createElement("h1");
        sideMenuTitleElement.classList.add("sidemenu-title");
        sideMenuTitleElement.innerHTML = menuConfig.location;
        sideMenuElement.appendChild(sideMenuTitleElement);
        
        let cardBoardElement = document.createElement("div");
        cardBoardElement.classList.add("card-board");
        sideMenuElement.appendChild(cardBoardElement);
        
        for(let [idx, cardData] of menuConfig.views.entries()) {
            let cardElement = this.__createCard(cardData);
            cardElement.setAttribute('id', `menu-${menu_index}-view-${idx}`);
            if(threeSixTea.sidebar.__lastSelectedCardId !== null &&
                threeSixTea.sidebar.__lastSelectedCardId === cardElement.id) {
                cardElement.classList.add('active');
            }            
            cardBoardElement.appendChild(cardElement);
        }
        
        return sideMenuElement;
    },
    
    load: function(language) {
        threeSixTea.sidebar.__language = language;
        threeSixTea.sidebar.init();
    },
    
    init: function() {
        let menuElement = document.getElementById("sidebar-menu");
        menuElement.innerHTML = "";
        
        for(let [idx, location] of threeSixTea.config.locations.entries()) {
            let element = this.__createMenu(idx, location);
            menuElement.appendChild(element);
        }
    }
}

threeSixTea.minimap = {
    __map_img: {},
    __map_canvas: {},
    __current_map_key: null,
    __marker_canvas: null,
    __view_direction_canvas: null,
    __position: {
        x: 0,
        y: 0
    },
    __showViewDirectionConfig: threeSixTea.config.minimap.showViewDirection || true,
    __rotation: 0,
    __rotationOffset: Math.PI,
    __timeoutId: null,
    
    __drawMap: function(mapKey) {
        let canvas = threeSixTea.minimap.__map_canvas[mapKey];
        let ctx = canvas.getContext("2d");
        let img = new Image();
        img.onload = function() {
            ctx.drawImage(img, 0, 0);
        }
        img.src = threeSixTea.minimap.__map_img[mapKey];
    },
    
    __hideMap: function(mapKey) {
        threeSixTea.minimap.__map_canvas[mapKey].classList.add("hide");
    },
    
    __showMap: function(mapKey) {
        threeSixTea.minimap.__map_canvas[mapKey].classList.remove("hide");
    },
    
    __drawViewDirection: function() {
        if(threeSixTea.minimap.__showViewDirectionConfig 
            && threeSixTea.minimap.__view_direction_canvas !== null) {
            let viewRadius = 40;
            let ctx = threeSixTea.minimap.__view_direction_canvas.getContext("2d");
            ctx.clearRect(0,0, threeSixTea.minimap.__view_direction_canvas.width, threeSixTea.minimap.__view_direction_canvas.height);
            
            ctx.save();
            ctx.translate(threeSixTea.minimap.__position.x, threeSixTea.minimap.__position.y);
            ctx.rotate(-1* (threeSixTea.minimap.__rotation + threeSixTea.minimap.__rotationOffset));
            
            let grd = ctx.createRadialGradient(0, 0, 10, 0, 0, viewRadius);
//             grd.addColorStop(0, "#0072c8ff");
//             grd.addColorStop(0.8, "#0072c860");
            grd.addColorStop(0,"#0074d3ff");
            grd.addColorStop(0.8, "#0074d360");
            grd.addColorStop(1, "transparent");
            
            
            ctx.fillStyle = grd;
            ctx.strokeStyle = grd;
            
            ctx.beginPath();
            ctx.moveTo(0,2*viewRadius);
            ctx.lineTo(0, 0);
            ctx.lineTo(2*viewRadius, 0);
            ctx.fill();
            ctx.restore();
        }
    },
    
    __clearViewDirection: function() {
        if(threeSixTea.minimap.__view_direction_canvas !== null) {
            let ctx = threeSixTea.minimap.__view_direction_canvas.getContext("2d");
            ctx.clearRect(0, 0, threeSixTea.minimap.__view_direction_canvas.width, threeSixTea.minimap.__view_direction_canvas.height);
        }
    },
    
    __hideViewDirection: function() {
        if(threeSixTea.minimap.__view_direction_canvas !== null) {
            threeSixTea.minimap.__view_direction_canvas.classList.add("hide");
        }
    },
    
    __showViewDirection: function() {
        if(threeSixTea.minimap.__view_direction_canvas !== null) {
            threeSixTea.minimap.__view_direction_canvas.classList.remove("hide");
        }
    },
    
    __drawMarker: function() {
        if(threeSixTea.minimap.__marker_canvas !== null) {
            let ctx = threeSixTea.minimap.__marker_canvas.getContext("2d");
            ctx.save();
            ctx.fillStyle = "#0084f0";

            ctx.beginPath();
            ctx.arc(threeSixTea.minimap.__position.x, threeSixTea.minimap.__position.y, 5, 0, 2*Math.PI);
            ctx.fill();
            ctx.closePath();
            ctx.restore();
        }
    },
    
    __clearMarker: function() {
        if(threeSixTea.minimap.__marker_canvas !== null) {
            let ctx = threeSixTea.minimap.__marker_canvas.getContext("2d");
            ctx.clearRect(threeSixTea.minimap.__position.x-8, threeSixTea.minimap.__position.y-8, 16, 16);
        }
    },
    
    __hideMarker: function() {
        if(threeSixTea.minimap.__marker_canvas !== null) {
            threeSixTea.minimap.__marker_canvas.classList.add("hide");
        }
    },
    
    __showMarker: function() {
        if(threeSixTea.minimap.__marker_canvas !== null) {
            threeSixTea.minimap.__marker_canvas.classList.remove("hide");
        }
    },
    
    __drawPotentialMarker: function(x ,y) {
        if(threeSixTea.minimap.__marker_canvas !== null) {
            let ctx = threeSixTea.minimap.__marker_canvas.getContext("2d");
            ctx.save();
            ctx.strokeStyle = "#cd1331";
            ctx.lineWidth = 2;

            ctx.beginPath();
            ctx.arc(x, y, 4, 0, 2*Math.PI);
            ctx.stroke();
            ctx.closePath();
        }
    },
    
    __clearPotentialMarker: function(x, y) {
        if(threeSixTea.minimap.__marker_canvas !== null) {
            let ctx = threeSixTea.minimap.__marker_canvas.getContext("2d");
            ctx.clearRect(x-8, y-8, 16, 16);
        }
    },
    
    setMarker: function(x, y) {
        if(threeSixTea.minimap.__marker_canvas !== null) {
            let ctx = threeSixTea.minimap.__marker_canvas.getContext("2d");
            ctx.clearRect(0, 0, threeSixTea.minimap.__marker_canvas.width, threeSixTea.minimap.__marker_canvas.height);
            
            threeSixTea.minimap.__position.x = x;
            threeSixTea.minimap.__position.y = y;
            
            threeSixTea.minimap.__drawMarker();
            threeSixTea.minimap.__showViewDirectionConfig = threeSixTea.config.minimap.showViewDirection;
            threeSixTea.minimap.__drawViewDirection();
        }
    },
    
    setViewRotationOffset: function(rotationOffset) {
        threeSixTea.minimap.callback(threeSixTea.minimap.__rotation - (rotationOffset - threeSixTea.minimap.__rotationOffset));
        threeSixTea.minimap.__rotationOffset = rotationOffset;
    },
    
    setViewDirection: function(radian) {
        threeSixTea.minimap.__rotation = radian;
        threeSixTea.minimap.__drawViewDirection();
    },
    
    callback: function(radian) {},
    
    displayMap: function(mapKey) {
        for(let key in threeSixTea.minimap.__map_canvas) {
            threeSixTea.minimap.__hideMap(key);
        }
        threeSixTea.minimap.__current_map_key = mapKey;
        threeSixTea.minimap.__showMap(mapKey);
    },
    
    displayPotentialMarker: function(markerData) {
        if(threeSixTea.minimap.__marker_canvas !== null) {
            
            if(threeSixTea.minimap.__position.x != markerData.x
                || threeSixTea.minimap.__position.y != markerData.y) {
            
                if(threeSixTea.minimap.__current_map_key !== markerData.minimap) {
                    threeSixTea.minimap.__hideMap(threeSixTea.minimap.__current_map_key);
                    threeSixTea.minimap.__showMap(markerData.minimap);
                    threeSixTea.minimap.__clearMarker();
                    threeSixTea.minimap.__showViewDirectionConfig = false;
                    threeSixTea.minimap.__clearViewDirection();

                    if(threeSixTea.minimap.__timeoutId !== null) {
                        window.clearTimeout(threeSixTea.minimap.__timeoutId);
                        threeSixTea.minimap.__timeoutId = null;
                    }
                }
                
                threeSixTea.minimap.__drawPotentialMarker(markerData.x, markerData.y);
            }
        }
    },
    
    hidePotentialMarker: function(markerData) {
        if(threeSixTea.minimap.__marker_canvas !== null) {
            if(threeSixTea.minimap.__position.x != markerData.x
                || threeSixTea.minimap.__position.y != markerData.y) {
                
                threeSixTea.minimap.__clearPotentialMarker(markerData.x, markerData.y);
                
                if(threeSixTea.minimap.__current_map_key === markerData.minimap) {
                    threeSixTea.minimap.__drawMarker();
                }
                
                // potential marker is on different map? Change back to current map!
                if(threeSixTea.minimap.__current_map_key !== markerData.minimap) {
                    threeSixTea.minimap.__timeoutId = window.setTimeout(function() {                   
                        threeSixTea.minimap.__showMap(threeSixTea.minimap.__current_map_key);
                        threeSixTea.minimap.__hideMap(markerData.minimap);
                        threeSixTea.minimap.__drawMarker();
                        threeSixTea.minimap.__showViewDirectionConfig = threeSixTea.config.minimap.showViewDirection || true;
                        threeSixTea.minimap.__drawViewDirection();
                    }, 200);
                }
            }
        }
    },
 
    init: function() {
        threeSixTea.minimap.__map_canvas["pws_map"] = document.getElementById("minimap-canvas-pws");
        threeSixTea.minimap.__map_canvas["aea_map"] = document.getElementById("minimap-canvas-aea");
        
        threeSixTea.minimap.__map_img["pws_map"] = "images/pws_map.png";
        threeSixTea.minimap.__map_img["aea_map"] = "images/aea_map.png";
        
        threeSixTea.minimap.__marker_canvas = document.getElementById("minimap-marker");
        threeSixTea.minimap.__view_direction_canvas = document.getElementById("minimap-view-direction");
        
        threeSixTea.minimap.__drawMap("pws_map");
        threeSixTea.minimap.__drawMap("aea_map");
    }
}


threeSixTea.debug = {
    
    __createRadianOffsetChanger: function(parent) {
        let debugRadianOffsetSlider = document.createElement("input");
        debugRadianOffsetSlider.type = "range";
        debugRadianOffsetSlider.min = 0;
        debugRadianOffsetSlider.max = 360;
        debugRadianOffsetSlider.value = 0;
        

        let tableRow = document.createElement("div");
        tableRow.classList.add("table-row");
        
        let labelCell = document.createElement("div");
        labelCell.classList.add("table-cell");
        labelCell.innerHTML = "Radian Offset";
        tableRow.appendChild(labelCell);
        
        let inputCell = document.createElement("div");
        inputCell.classList.add("table-cell");
        inputCell.appendChild(debugRadianOffsetSlider);
        tableRow.appendChild(inputCell);
        
        let valueCell = document.createElement("div");
        valueCell.classList.add("table-cell");
        valueCell.innerHTML = debugRadianOffsetSlider.value;
        tableRow.appendChild(valueCell);
        
        
        debugRadianOffsetSlider.addEventListener("input", function(event) {
            let value = event.target.value;
            valueCell.innerHTML = value;
            threeSixTea.minimap.__rotationOffset = (value*Math.PI)/180;
            threeSixTea.minimap.__drawViewDirection();
        });
        
        
        parent.appendChild(tableRow);
    },
    
    init: function(active=false) {
        if(btoa(threeSixTea.common.getURLParam("debug")) === "dGhyZWUtc2l4LXRlYS10aW1l" 
            && (location.hostname === "localhost" || location.hostname === "127.0.0.1") ) {
            let debugElement = document.createElement("div");
            debugElement.id = "debug";
            document.body.appendChild(debugElement);
            
            let debugTable = document.createElement("div");
            debugTable.classList.add("table");
            debugElement.appendChild(debugTable);
            
            threeSixTea.debug.__createRadianOffsetChanger(debugTable);
        }
    },
}

window.onload = function() {
    threeSixTea.vr.init();
    threeSixTea.sidebar.init();
    threeSixTea.minimap.init();
    
    threeSixTea.debug.init(true);
    
    
    // initial content
    if(threeSixTea.config.locations != undefined) {
        let firstLocation = threeSixTea.config.locations[0];
        let views = firstLocation.views
        if(views != undefined) {
            threeSixTea.sidebar.__onClick(views[0]);
        }
    }
}

