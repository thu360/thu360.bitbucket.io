var threeSixTea = threeSixTea || {};

function toRadian(degree) {
    return (degree*Math.PI)/180;
}

threeSixTea.config = {
    startViewDirection: toRadian(125),
    resources: {
        imagesDir: "assets/images",
        thumbsDir: "assets/thumbs",
    },
    minimap: {
        showViewDirection: true
    },
    locations: [
        {
            location: "Prittwitzstraße",
            views: [
				{
                    name: {
                        "de": "Eingang Mensa im Winter",
                        "en": "Entrance Mensa in Winter",
                    },
                    sub: { 
                        "de": "Außen",
                        "en": "Outside",
                    },
                    thumb: "pws_entrance_mensa_winter_night.jpg",
                    image: "pws_entrance_mensa_winter_night.jpg",
                    marker: { 
                        minimap: "pws_map",
                        x: 252, 
                        y: 236,
                        rotationOffset: toRadian(120) 
                    }
                },{
                    name: {
                        "de": "A-Bau Blick nach Süden",
                        "en": "Building A South View",
                    },
                    sub: { 
                        "de": "Außen",
                        "en": "Outside",
                    },
                    thumb: "pws_building_a_south_view.jpg",
                    image: "pws_building_a_south_view.jpg",
                    marker: { 
                        minimap: "pws_map",
                        x: 108, 
                        y: 164,
                    rotationOffset: toRadian(155)
                    }
                },{
                    name: {
                        "de": "Behinderten & Fahrrad Parkplatz",
                        "en": "Car Park for Disabled People and Bikes",
                    },
                    sub: {
                        "de": "Außen",
                        "en": "Outside",
                    },
                    thumb: "pws_innenhof.jpg",
                    image: "pws_innenhof.jpg",
                    marker: { 
                        minimap: "pws_map",
                        x: 136, 
                        y: 200,
                        rotationOffset: toRadian(72)
                    }
                }, {
                    name: {
                        "de": "Großer Innenhof",
                        "en": "Large Courtyard",
                    },
                    sub: { 
                        "de": "Außen",
                        "en": "Outside",
                    },
                    thumb: "pws_large_courtyard.jpg",
                    image: "pws_large_courtyard.jpg",
                    marker: { 
                        minimap: "pws_map",
                        x: 180, 
                        y: 194,
                        rotationOffset: toRadian(285)
                    }
                }, {
                    name: {
                        "de": "Briefkästen A-Bau",
                        "en": "Postbox Building A",
                    },
                    sub: {
                        "de": "Erdgeschoss",
                        "en": "Ground Floor",
                    },
                    thumb: "pws_a_bau_briefkaesten.jpg",
                    image: "pws_a_bau_briefkaesten.jpg",
                    marker: { 
                        minimap: "pws_map",
                        x: 138, 
                        y: 146,
                        rotationOffset: toRadian(45)
                    }
                }, {
                    name: {
                        "de": "Foyer A-Bau",
                        "en": "Lobby Building A",
                    },
                    sub: {
                        "de": "Erdgeschoss",
                        "en": "Ground Floor",
                    },
                    thumb: "pws_briefkaesten_2.jpg",
                    image: "pws_briefkaesten_2.jpg",
                    marker: { 
                        minimap: "pws_map",
                        x: 143, 
                        y: 144,
                        rotationOffset: toRadian(145)
                    }
                }, {
                    name: {
                        "de": "Lernbereich A-Bau",
                        "en": "Study Area Building A",
                    },
                    sub: {
                        "de": "Erdgeschoss",
                        "en": "Ground Floor",
                    },
                    thumb: "pws_a_bau_treppe_1.jpg",
                    image: "pws_a_bau_treppe_1.jpg",
                    marker: { 
                        minimap: "pws_map",
                        x: 163, 
                        y: 134,
                        rotationOffset: toRadian(332)
                    }
                }, {
                    name: {
                        "de": "A-Bau zu C-Bau",
                        "en": "Building A to C",
                    },
                    sub: {
                        "de": "Erdgeschoss",
                        "en": "Ground Floor",
                    },
                    thumb: "pws_a_bau_treppe_2.jpg",
                    image: "pws_a_bau_treppe_2.jpg",
                    marker: { 
                        minimap: "pws_map",
                        x: 184, 
                        y: 128,
                        rotationOffset: toRadian(70)
                    }
                }, {
                    name: {
                        "de": "Automaten C-Bau",
                        "en": "Automata Building C",
                    },
                    sub: { 
                        "de": "Untergeschoss",
                        "en": "Basement",
                    },
                    thumb: "pws_automata_building_c.jpg",
                    image: "pws_automata_building_c.jpg",
                    marker: { 
                        minimap: "pws_map",
                        x: 177, 
                        y: 131,
                        rotationOffset: toRadian(305)
                    }
                }, {
                    name: {
                        "de": "Flur D-Bau",
                        "en": "Floor Building D",
                    },
                    sub: {
                        "de": "Untergeschoss",
                        "en": "Basement",
                    },
                    thumb: "pws_einsteinracing.jpg",
                    image: "pws_einsteinracing.jpg",
                    marker: { 
                        minimap: "pws_map",
                        x: 208, 
                        y: 129,
                        rotationOffset: toRadian(314)
                    }
                },{
                    name: { 
                        "de": "THU Studio",
                        "en": "THU Studio",
                    },
                    sub: { 
                        "de": "Untergeschoss",
                        "en": "Basement",
                    },
                    thumb: "pws_studio.jpg",
                    image: "pws_studio.jpg",
                    marker: { 
                        minimap: "pws_map",
                        x: 177, 
                        y: 143,
                        rotationOffset: toRadian(130)
                    }
                }, {
                    name: {
                        "de": "Flur C-Bau",
                        "en": "Floor Building C",
                    },
                    sub: {
                        "de": "Erdgeschoss",
                        "en": "Ground Floor",
                    },
                    thumb: "pws_korridor_3.jpg",
                    image: "pws_korridor_3.jpg",
                    marker: { 
                        minimap: "pws_map",
                        x: 189, 
                        y: 166,
                        rotationOffset: toRadian(230)
                    }
                }, {
                    name: {
                        "de": "B-Bau zu C-Bau",
                        "en": "Building B to C",
                    },
                    sub: {
                        "de": "Untergeschoss",
                        "en": "Basement",
                    },
                    thumb: "pws_b_bau_vorbibliothek.jpg",
                    image: "pws_b_bau_vorbibliothek.jpg",
                    marker: { 
                        minimap: "pws_map",
                        x: 202, 
                        y: 204,
                        rotationOffset: toRadian(136)
                    }
                }, {
                    name: {
                        "de": "Eingang B-Bau",
                        "en": "Lobby Building B",
                    },
                    sub: {
                        "de": "Untergeschoss",
                        "en": "Basement",
                    },
                    thumb: "pws_aula_eingang_1.jpg",
                    image: "pws_aula_eingang_1.jpg",
                    marker: { 
                        minimap: "pws_map",
                        x: 168, 
                        y: 218,
                        rotationOffset: toRadian(55)
                    }
                },{
                    name: {
                        "de": "Eingang B-Bau",
                        "en": "Entrance Building B",
                    },
                    sub: {
                        "de": "Untergeschoss",
                        "en": "Basement",
                    },
                    thumb: "pws_b_bau_eingang.jpg",
                    image: "pws_b_bau_eingang.jpg",
                    marker: { 
                        minimap: "pws_map",
                        x: 185, 
                        y: 211,
                        rotationOffset: toRadian(55)
                    }
                }, {
                    name: {
                        "de": "Lernbereich B-Bau",
                        "en": "Study Area Building B",
                    },
                    sub: {
                        "de": "Untergeschoss",
                        "en": "Basement",
                    },
                    thumb: "pws_aula_eingang_2.jpg",
                    image: "pws_aula_eingang_2.jpg",
                    marker: { 
                        minimap: "pws_map",
                        x: 156, 
                        y: 221,
                        rotationOffset: toRadian(107)
                    }
                },  {
                    name: {
                        "de": "Infotafeln B-Bau",
                        "en": "Info Panels Building B",
                    },
                    sub: {
                        "de": "Erdgeschoss",
                        "en": "Ground Floor",
                    },
                    thumb: "pws_b_bau_etage1.jpg",
                    image: "pws_b_bau_etage1.jpg",
                    marker: { 
                        minimap: "pws_map",
                        x: 194, 
                        y: 208,
                        rotationOffset: toRadian(256)
                    }
                },{
                    name: {
                        "de": "Ausweisautomat B-Bau",
                        "en": "ID Card Machine Building B",
                    },
                    sub: {
                        "de": "Erdgeschoss",
                        "en": "Ground Floor",
                    },
                    thumb: "pws_b_bau_drucker.jpg",
                    image: "pws_b_bau_drucker.jpg",
                    marker: { 
                        minimap: "pws_map",
                        x: 156, 
                        y: 221,
                        rotationOffset: toRadian(182)
                    }
                }, {
                    name: {
                        "de": "Aula Eingang",
                        "en": "Auditorium Entrance",
                    },
                    sub: { 
                        "de": "Untergeschoss",
                        "en": "Basement",
                    },
                    thumb: "pws_auditorium_entrance.jpg",
                    image: "pws_auditorium_entrance.jpg",
                    marker: { 
                        minimap: "pws_map",
                        x: 166, 
                        y: 223,
                        rotationOffset: toRadian(64)
                    }
                }, {
                    name: {
                        "de": "Aula Front",
                        "en": "Auditorium Front",
                    },
                    sub: { 
                        "de": "Untergeschoss",
                        "en": "Basement",
                    },
                    thumb: "pws_auditorium_front.jpg",
                    image: "pws_auditorium_front.jpg",
                    marker: { 
                        minimap: "pws_map",
                        x: 172, 
                        y: 245,
                        rotationOffset: toRadian(300)
                    }
                }, {
                    name: { 
                        "de": "Eingang Bibliothek",
                        "en": "Entrance Library",
                    },
                    sub: { 
                        "de": "Untergeschoss",
                        "en": "Basement",
                    },
                    thumb: "pws_entrance_library.jpg",
                    image: "pws_entrance_library.jpg",
                    marker: { 
                        minimap: "pws_map",
                        x: 224, 
                        y: 197,
                        rotationOffset: toRadian(320)
                    }
                }, {
                    name: {
                        "de": "Bibliothek",
                        "en": "Library",
                    },
                    sub: {
                        "de": "Untergeschoss",
                        "en": "Basement",
                    },
                    thumb: "pws_bibliothek_eingang.jpg",
                    image: "pws_bibliothek_eingang.jpg",
                    marker: { 
                        minimap: "pws_map",
                        x: 230, 
                        y: 206,
                        rotationOffset: toRadian(335)
                    }
                },{
                    name: {
                        "de": "Bibliothek",
                        "en": "Library",
                    },
                    sub: {
                        "de": "Erdgeschoss",
                        "en": "First Floor",
                    },
                    thumb: "pws_bibliothek_oben.jpg",
                    image: "pws_bibliothek_oben.jpg",
                    marker: { 
                        minimap: "pws_map",
                        x: 233, 
                        y: 206,
                        rotationOffset: toRadian(152)
                    }
                },{
                    name: {
                        "de": "Eingang Cafeteria",
                        "en": "Entrance Cafeteria",
                    },
                    sub: { 
                        "de": "Untergeschoss",
                        "en": "Basement",
                    },
                    thumb: "pws_entrance_cafeteria.jpg",
                    image: "pws_entrance_cafeteria.jpg",
                    marker: { 
                        minimap: "pws_map",
                        x: 234, 
                        y: 195,
                        rotationOffset: toRadian(0)
                    }
                }, {
                    name: {
                        "de": "Cafeteria",
                        "en": "Cafeteria",
                    },
                    sub: { 
                        "de": "Untergeschoss",
                        "en": "Basement",
                    },
                    thumb: "pws_cafeteria_1.jpg",
                    image: "pws_cafeteria_1.jpg",
                    marker: { 
                        minimap: "pws_map",
                        x: 235, 
                        y: 203,
                        rotationOffset: toRadian(321)
                    }
                }, {
                    name: {
                        "de": "Cafeteria Kasse",
                        "en": "Cafeteria Checkout",
                    },
                    sub: { 
                        "de": "Untergeschoss",
                        "en": "Basement",
                    },
                    thumb: "pws_cafeteria_4.jpg",
                    image: "pws_cafeteria_4.jpg",
                    marker: { 
                        minimap: "pws_map",
                        x: 244, 
                        y: 202,
                        rotationOffset: toRadian(145)
                    }
                },{
                    name: {
                        "de": "Lernecke B-Bau",
                        "en": "Study Corner Building B",
                    },
                    sub: {
                        "de": "Erdgeschoss",
                        "en": "Ground Floor",
                    },
                    thumb: "pws_b_bau_flur_5.jpg",
                    image: "pws_b_bau_flur_5.jpg",
                    marker: { 
                        minimap: "pws_map",
                        x: 241, 
                        y: 192,
                        rotationOffset: toRadian(136)
                    }
                }, {
                    name: {
                        "de": "Mikrocontroller Labor C04",
                        "en": "Microcontroller Lab C04",
                    },
                    sub: {
                        "de": "Erdgeschoss",
                        "en": "Ground Floor",
                    },
                    thumb: "pws_pc_labor_1.jpg",
                    image: "pws_pc_labor_1.jpg",
                    marker: { 
                        minimap: "pws_map",
                        x: 237, 
                        y: 172,
                        rotationOffset: toRadian(244)
                    }
                }, {
                    name: {
                        "de": "Eingang Mensa",
                        "en": "Entrance Mensa",
                    },
                    sub: { 
                        "de": "Außen",
                        "en": "Outside",
                    },
                    thumb: "pws_entrance_mensa.jpg",
                    image: "pws_entrance_mensa.jpg",
                    marker: { 
                        minimap: "pws_map",
                        x: 252, 
                        y: 236,
                        rotationOffset: toRadian(120) 
                    }
                },{
                    name: {
                        "de": "Eingang Mensa",
                        "en": "Entrance Mensa",
                    },
                    sub: {
                        "de": "Erdgeschoss",
                        "en": "Ground Floor",
                    },
                    thumb: "pws_mensa_eingang.jpg",
                    image: "pws_mensa_eingang.jpg",
                    marker: { 
                        minimap: "pws_map",
                        x: 266, 
                        y: 233,
                        rotationOffset: toRadian(240)
                    }
                },  {
                    name: { 
                        "de": "Eingang B-Bau",
                        "en": "Entrance Building B",
                    },
                    sub: { 
                        "de": "Außen",
                        "en": "Outside",
                    },
                    thumb: "pws_entrance_building_b.jpg",
                    image: "pws_entrance_building_b.jpg",
                    marker: { 
                        minimap: "pws_map",
                        x: 202, 
                        y: 227,
                        rotationOffset: toRadian(87)
                    }
                }, {
                    name: {
                        "de": "Wiesenbereich",
                        "en": "Meadow Area",
                    },
                    sub: {
                        "de": "Außen",
                        "en": "Outside",
                    },
                    thumb: "pws_b_bau_wiese_2.jpg",
                    image: "pws_b_bau_wiese_2.jpg",
                    marker: { 
                        minimap: "pws_map",
                        x: 228, 
                        y: 264,
                        rotationOffset: toRadian(340)
                    }
                }, {
                    name: {
                        "de": "Parkplatz für Studierende",
                        "en": "Car Park for Students",
                    },
                    sub: {
                        "de": "Außen",
                        "en": "Outside",
                    },
                    thumb: "pws_parkplatz.jpg",
                    image: "pws_parkplatz.jpg",
                    marker: { 
                        minimap: "pws_map",
                        x: 45, 
                        y: 240,
                        rotationOffset: toRadian(200)
                    }
                }, {
                    name: {
                        "de": "Büro Personalabteilung",
                        "en": "Personnel Office",
                    },
                    sub: {
                        "de": "Erdgeschoss",
                        "en": "Ground Floor",
                    },
                    thumb: "pws_ssc_korridor_2.jpg",
                    image: "pws_ssc_korridor_2.jpg",
                    marker: { 
                        minimap: "pws_map",
                        x: 150, 
                        y: 212,
                        rotationOffset: toRadian(56)
                    }
                }, {
                    name: { 
                        "de": "Büro Duale Studiengänge",
                        "en": "Office Dual Studies",
                    },
                    sub: {
                        "de": "Erdgeschoss",
                        "en": "First Floor",
                    },
                    thumb: "pws_ssc_korridor_4.jpg",
                    image: "pws_ssc_korridor_4.jpg",
                    marker: { 
                        minimap: "pws_map",
                        x: 137, 
                        y: 174,
                        rotationOffset: toRadian(30)
                    }
                }, {
                    name: {
                        "de": "Büro SSC",
                        "en": "Office SSC",
                    },
                    sub: {
                        "de": "Erdgeschoss",
                        "en": "First Floor",
                    },
                    thumb: "pws_ssc_korridor_6.jpg",
                    image: "pws_ssc_korridor_6.jpg",
                    marker: { 
                        minimap: "pws_map",
                        x: 134, 
                        y: 168,
                        rotationOffset: toRadian(60)
                    }
                },  {
                    name: {
                        "de": "Überdachung A-Bau zu F-Bau",
                        "en": "Roofing Building A to F",
                    },
                    sub: {
                        "de": "Außen",
                        "en": "Outside",
                    },
                    thumb: "pws_f_bau_draussen.jpg",
                    image: "pws_f_bau_draussen.jpg",
                    marker: { 
                        minimap: "pws_map",
                        x: 118, 
                        y: 125,
                        rotationOffset: toRadian(155)
                    }
                },{
                    name: {
                        "de": "Eingang F-Bau",
                        "en": "Entrance Building F",
                    },
                    sub: {
                        "de": "Erdgeschoss",
                        "en": "Ground Floor",
                    },
                    thumb: "pws_f_bau_eingang.jpg",
                    image: "pws_f_bau_eingang.jpg",
                    marker: { 
                        minimap: "pws_map",
                        x: 100, 
                        y: 111,
                        rotationOffset: toRadian(164)
                    }
                },{
                    name: {
                        "de": "Flur F-Bau",
                        "en": "Floor Buidling F",
                    },
                    sub: {
                        "de": "Erdgeschoss",
                        "en": "Ground Floor",
                    },
                    thumb: "pws_f_bau_eingang_2.jpg",
                    image: "pws_f_bau_eingang_2.jpg",
                    marker: { 
                        minimap: "pws_map",
                        x: 110, 
                        y: 109,
                        rotationOffset: toRadian(164)
                    }
                },{
                    name: {
                        "de": "PC-Pool F-Bau",
                        "en": "PC Pool Building F",
                    },
                    sub: {
                        "de": "Erdgeschoss",
                        "en": "Ground Floor",
                    },
                    thumb: "pws_f_bau_kopierraum_1.jpg",
                    image: "pws_f_bau_kopierraum_1.jpg",
                    marker: { 
                        minimap: "pws_map",
                        x: 128, 
                        y: 98,
                        rotationOffset: toRadian(290)
                    }
                },{
                    name: {
                        "de": "PC-Pool F-Bau Hinten",
                        "en": "PC Pool Building F Back",
                    },
                    sub: {
                        "de": "Erdgeschoss",
                        "en": "Ground Floor",
                    },
                    thumb: "pws_f_bau_kopierraum_3.jpg",
                    image: "pws_f_bau_kopierraum_3.jpg",
                    marker: { 
                        minimap: "pws_map",
                        x: 126, 
                        y: 90,
                        rotationOffset: toRadian(18)
                    }
                },{
                    name: {
                        "de": "Parkplatz für Angestellte",
                        "en": "Car Park for Employees",
                    },
                    sub: {
                        "de": "Außen",
                        "en": "Outside",
                    },
                    thumb: "pws_f_bau_parkplatz.jpg",
                    image: "pws_f_bau_parkplatz.jpg",
                    marker: { 
                        minimap: "pws_map",
                        x: 158, 
                        y: 100,
                        rotationOffset: toRadian(314)
                    }
                },{
                    name: {
                        "de": "Computer Raum F-Bau",
                        "en": "Computer Room Building F",
                    },
                    sub: {
                        "de": "Erdgeschoss",
                        "en": "Ground Floor",
                    },
                    thumb: "pws_f_bau_pc_pool.jpg",
                    image: "pws_f_bau_pc_pool.jpg",
                    marker: { 
                        minimap: "pws_map",
                        x: 120, 
                        y: 97,
                        rotationOffset: toRadian(245)
                    }
                }
            ]
        }, {
            location: "Albert-Einstein-Allee",
            views: [
                {
                    name: {
                        "de": "Parkplatz",
                        "en": "Car Park",
                    },
                    sub: {
                        "de": "Außen",
                        "en": "Outside",
                    },
                    thumb: "aea_eingang_parkplatz.jpg",
                    image: "aea_eingang_parkplatz.jpg",
                    marker: { 
                        minimap: "aea_map",
                        x: 25, 
                        y: 116,
                        rotationOffset: toRadian(32)
                    }
                },{
                    name: {
                        "de": "Eingang V-Bau",
                        "en": "Entrance Building V",
                    },
                    sub: {
                        "de": "Erdgeschoss",
                        "en": "Ground Floor",
                    },
                    thumb: "aea_eingang_v_bau.jpg",
                    image: "aea_eingang_v_bau.jpg",
                    marker: { 
                        minimap: "aea_map",
                        x: 28, 
                        y: 152,
                        rotationOffset: toRadian(130)
                    }
                }, {
                    name: {
                        "de": "Sekretariat V-Bau",
                        "en": "Secretariat Buidling V",
                    },
                    sub: {
                        "de": "Erdgeschoss",
                        "en": "Ground Floor",
                    },
                    thumb: "aea_foyer_v_bau.jpg",
                    image: "aea_foyer_v_bau.jpg",
                    marker: { 
                        minimap: "aea_map",
                        x: 38, 
                        y: 151,
                        rotationOffset: toRadian(135)
                    }
                }, {
                    name: {
                        "de": "Infotafeln Wahlfächer Q-Bau",
                        "en": "Info Panels Electives Building Q",
                    },
                    sub: {
                        "de": "Erdgeschoss",
                        "en": "Ground Floor",
                    },
                    thumb: "aea_korridor_1.jpg",
                    image: "aea_korridor_1.jpg",
                    marker: { 
                        minimap: "aea_map",
                        x: 52, 
                        y: 152,
                        rotationOffset: toRadian(149)
                    }
                },{
                    name: {
                        "de": "Infotafeln Praxissemester Q-Bau",
                        "en": "Info Panels Internship Building Q",
                    },
                    sub: {
                        "de": "Erdgeschoss",
                        "en": "Ground Floor",
                    },
                    thumb: "aea_korridor_2.jpg",
                    image: "aea_korridor_2.jpg",
                    marker: { 
                        minimap: "aea_map",
                        x: 78, 
                        y: 152,
                        rotationOffset: toRadian(106)
                    }
                },{
                    name: {
                        "de": "Lernbereich Q-Bau",
                        "en": "Study Area Building Q",
                    },
                    sub: {
                        "de": "Erdgeschoss",
                        "en": "Ground Floor",
                    },
                    thumb: "aea_korridor_3.jpg",
                    image: "aea_korridor_3.jpg",
                    marker: { 
                        minimap: "aea_map",
                        x: 93, 
                        y: 152,
                        rotationOffset: toRadian(145)
                    }
                },{
                    name: {
                        "de": "Q-Bau zu S-Bau",
                        "en": "Building Q to S",
                    },
                    sub: {
                        "de": "Erdgeschoss",
                        "en": "Ground Floor",
                    },
                    thumb: "aea_korridor_4.jpg",
                    image: "aea_korridor_4.jpg",
                    marker: { 
                        minimap: "aea_map",
                        x: 108, 
                        y: 152,
                        rotationOffset: toRadian(129)
                    }
                },{
                    name: {
                        "de": "Ausweisautomat Q-Bau",
                        "en": "ID Card Machine Building Q",
                    },
                    sub: {
                        "de": "Erdgeschoss",
                        "en": "Ground Floor",
                    },
                    thumb: "aea_korridor_5.jpg",
                    image: "aea_korridor_5.jpg",
                    marker: { 
                        minimap: "aea_map",
                        x: 165, 
                        y: 152,
                        rotationOffset: toRadian(128)
                    }
                },{
                    name: {
                        "de": "Eingang Bibliothek",
                        "en": "Entrance Library",
                    },
                    sub: {
                        "de": "Erdgeschoss",
                        "en": "Ground Floor",
                    },
                    thumb: "aea_eingang_bibliothek.jpg",
                    image: "aea_eingang_bibliothek.jpg",
                    marker: { 
                        minimap: "aea_map",
                        x: 172, 
                        y: 159,
                        rotationOffset: toRadian(152)
                    }
                }, {
                    name: {
                        "de": "Vorlesungssaal Q102 Front",
                        "en": "Lecture Hall Q102 Front",
                    },
                    sub: {
                        "de": "Erdgeschoss",
                        "en": "Ground Floor",
                    },
                    thumb: "aea_grosser_vorlesungssaal_1.jpg",
                    image: "aea_grosser_vorlesungssaal_1.jpg",
                    marker: { 
                        minimap: "aea_map",
                        x: 190, 
                        y: 157,
                        rotationOffset: toRadian(160)
                    }
                },{
                    name: {
                        "de": "Vorlesungssaal Q102 Hinten",
                        "en": "Lecture Hall Q102 Back",
                    },
                    sub: {
                        "de": "Erdgeschoss",
                        "en": "Ground Floor",
                    },
                    thumb: "aea_grosser_vorlesungssaal_2.jpg",
                    image: "aea_grosser_vorlesungssaal_2.jpg",
                    marker: { 
                        minimap: "aea_map",
                        x: 193, 
                        y: 162,
                        rotationOffset: toRadian(117)
                    }
                },{
                    name: {
                        "de": "Eingang Cafeteria",
                        "en": "Entrance Cafeteria",
                    },
                    sub: {
                        "de": "Untergeschoss",
                        "en": "Basement",
                    },
                    thumb: "aea_cafeteria_entrance.jpg",
                    image: "aea_cafeteria_entrance.jpg",
                    marker: { 
                        minimap: "aea_map",
                        x: 170, 
                        y: 157,
                        rotationOffset: toRadian(330)
                    }
                }, {
                    name: {
                        "de": "Mensa Sitzbereich",
                        "en": "Mensa Seating Area",
                    },
                    sub: {
                        "de": "Untergeschoss",
                        "en": "Basement",
                    },
                    thumb: "aea_cafeteria_sitzbereich.jpg",
                    image: "aea_cafeteria_sitzbereich.jpg",
                    marker: { 
                        minimap: "aea_map",
                        x: 160, 
                        y: 158,
                        rotationOffset: toRadian(305)
                    }
                }, {
                    name: {
                        "de": "Mensa Sitzbereich",
                        "en": "Mensa Seating Area",
                    },
                    sub: { 
                        "de": "Außen",
                        "en": "Outside",
                    },
                    thumb: "aea_mensa_seating_outside.jpg",
                    image: "aea_mensa_seating_outside.jpg",
                    marker: { 
                        minimap: "aea_map",
                        x: 144, 
                        y: 175,
                        rotationOffset: toRadian(100)
                    }
                }
            ]
        }
    ]
};
