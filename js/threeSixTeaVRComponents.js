AFRAME.registerComponent('camera-listener', {
    tick: function() {
        window.parent.threeSixTea.minimap.setViewDirection(this.el.object3D.rotation._y);
    },
    init: function() {
        let camera = this.el;
        camera.components['look-controls'].yawObject.rotation.y = window.parent.threeSixTea.config.startViewDirection;
        window.parent.threeSixTea.minimap.callback = function(radian) {
            camera.setAttribute('look-controls', {enabled: false});
            camera.components['look-controls'].yawObject.rotation.y = radian;
            camera.setAttribute('look-controls', {enabled: true});
        };
    },
});

AFRAME.registerComponent('image-changer', {
    init: function() {
        let skybox = this.el;
        let newImage = null;

        window.parent.threeSixTea.vr.changeImage = function(image) {
            newImage=`${window.parent.threeSixTea.config.resources.imagesDir}/${image}`;

            if(newImage !== skybox.getAttribute("src")) {
                skybox.emit('fadeOut');
            }
        };
        
        skybox.addEventListener('animationcomplete__fade_out', function(e) {
            skybox.setAttribute("src", newImage);
        });
    },
});
